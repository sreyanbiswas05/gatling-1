package simulations

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.language.postfixOps

class fixedDurationLoadSimulation extends Simulation{

  val httpConfig = http.baseUrl("https://reqres.in/")
    .header("Accept","application/json")
    .header("content-type","application/json")

  def getAllUserRequest()={
    repeat(2){
      exec(http("get all user")
        .get("api/users?page=2")
        .check(status is 200))
    }
  }

  def getSingleUserRequest()={
    repeat(2){
      exec(http("get single user")
        .get("api/users/2")
        .check(status.is(200)))
    }
  }

  def addUser()={
    repeat(2){
      exec(http("add user")
        .post("api/users")
        .body(RawFileBody("./src/test/resources/bodies/addUser.json"))
        .check(status.is(201)))
    }
  }

  val scn = scenario("fixed duration load simulation")
    .forever(){
         exec(getAllUserRequest())
        .pause(3)
        .exec(getSingleUserRequest())
        .pause(3)
        .exec(addUser())
    }

  setUp(
    scn.inject(
      nothingFor(5),
      atOnceUsers(10),
      rampUsers(50) during(30)
    ).protocols(httpConfig)
  ).maxDuration(1 minutes)

}
