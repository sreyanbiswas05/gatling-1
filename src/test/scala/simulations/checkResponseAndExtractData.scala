package simulations

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class checkResponseAndExtractData extends Simulation{

  val httpConfig = http.baseUrl("https://gorest.co.in")
    .header("Authorization","Bearer 29d984f7cae1ede2a683a23798ffd8102dbf4b5e7367080c4ec7158e469006f1")

  val scn = scenario("Check correlation & extract data")
    .exec(http("get all users api")
    .get("/public-api/users")
    .check(jsonPath("$.data[0].id").saveAs("userId")))

    .exec(http("get single user api")
    .get("/public-api/users/${userId}")
    .check(jsonPath("$.data.id").is("3864"))
    .check(jsonPath("$.data.name").is("Vaishvi Tandon"))
      .check(jsonPath("$.data.email").is("tandon_vaishvi@friesen.com"))
    )

  setUp(scn.inject(atOnceUsers(1))).protocols(httpConfig)

}
