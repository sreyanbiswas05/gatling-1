package simulations

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._
import scala.language.postfixOps

class loadSimulationBasic extends Simulation {

  val httpConfig = http.baseUrl("https://gorest.co.in/")
    .header("Authorization","Bearer 29d984f7cae1ede2a683a23798ffd8102dbf4b5e7367080c4ec7158e469006f1")

  val csvFeeder = csv("./src/test/resources/data/getUser.csv").circular

  def getAUser()={
    repeat(1){
      feed(csvFeeder)
        .exec(http("get single user request")
          .get("public/v2/users/${userid}")
          .check(jsonPath("$.name").is("${name}"))
          .check(status.in(200,304)))
        .pause(2)
    }
  }

  val scn = scenario("get a user").exec(getAUser())

  setUp(
    scn.inject(
      nothingFor(5),
      atOnceUsers(5),
      rampUsers(10) during(10 seconds)
    )
  ).protocols(httpConfig)

}
