package simulations

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class updateAndDeleteUserSimulation extends Simulation {

  val httpConfig = http.baseUrl("https://reqres.in")
    .header("Accept","application/json")
    .header("content-type","application/json")

  val scn = scenario("update user api")
    //first updating the user
    .exec(http("updating user")
    .put("/api/users/2")
    .body(RawFileBody("./src/test/resources/bodies/updateUser.json")).asJson
    .check(status.in(200 to 201)))

    .pause(3)
    //second deleting the user
    .exec(http("deleting user")
    .delete("/api/users/2")
    .check(status.in(200 to 210)))

  setUp(scn.inject(atOnceUsers(1))).protocols(httpConfig)

}
