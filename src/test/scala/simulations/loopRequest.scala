package simulations

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class loopRequest extends Simulation {

  val httpConfig = http.baseUrl("https://reqres.in/")
    .header("Accept","application/json")
    .header("content-type","application/json")

  def getAllUserRequest()={
    repeat(2){
      exec(http("get all user")
        .get("api/users?page=2")
        .check(status is 200))
    }
  }

  def getSingleUserRequest()={
    repeat(2){
      exec(http("get single user")
      .get("api/users/2")
      .check(status.is(200)))
    }
  }

  def addUser()={
    repeat(2){
       exec(http("add user")
      .post("api/users")
      .body(RawFileBody("./src/test/resources/bodies/addUser.json"))
      .check(status.is(201)))
    }
  }

  val scn = scenario("get user request")
    .exec(getAllUserRequest())
    .pause(3)
    .exec(getSingleUserRequest())
    .pause(3)
    .exec(addUser())

  setUp(scn.inject(atOnceUsers(1))).protocols(httpConfig)

}
