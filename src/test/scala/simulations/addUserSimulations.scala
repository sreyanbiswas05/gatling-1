package simulations

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class addUserSimulations extends Simulation {

  val httpConfig = http.baseUrl("https://reqres.in")
    .header("Accept",value="application/json")
    .header("content-type",value = "application/json")

  val scn = scenario("add user data")
    .exec(http("add user data")
    .post("/api/users")
    .body(RawFileBody("./src/test/resources/bodies/addUser.json")).asJson
      .header("content-type","application/json")
      .check(status is 201))

    .pause(3)

    .exec(http("get user request")
    .get("/api/users/2")
    .check(status is 200))

    .pause(2)

    .exec(http("get all user request")
    .get("/api/users?page=2")
    .check(status is 200))

  setUp(scn.inject(atOnceUsers(1))).protocols(httpConfig)
}
