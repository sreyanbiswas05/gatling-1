var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "562",
        "ok": "562",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles1": {
        "total": "562",
        "ok": "562",
        "ko": "-"
    },
    "percentiles2": {
        "total": "665",
        "ok": "665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "747",
        "ok": "747",
        "ko": "-"
    },
    "percentiles4": {
        "total": "764",
        "ok": "764",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.4",
        "ok": "0.4",
        "ko": "-"
    }
},
contents: {
"req_updating-user-d5346": {
        type: "REQUEST",
        name: "updating user",
path: "updating user",
pathFormatted: "req_updating-user-d5346",
stats: {
    "name": "updating user",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "percentiles2": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "percentiles3": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "percentiles4": {
        "total": "768",
        "ok": "768",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.2",
        "ok": "0.2",
        "ko": "-"
    }
}
    },"req_deleting-user-d1ea1": {
        type: "REQUEST",
        name: "deleting user",
path: "deleting user",
pathFormatted: "req_deleting-user-d1ea1",
stats: {
    "name": "deleting user",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "percentiles2": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "percentiles3": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "percentiles4": {
        "total": "355",
        "ok": "355",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.2",
        "ok": "0.2",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
