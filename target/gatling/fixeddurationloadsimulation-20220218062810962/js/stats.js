var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "829",
        "ok": "829",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "876",
        "ok": "876",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "318",
        "ok": "318",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "121",
        "ok": "121",
        "ko": "-"
    },
    "percentiles1": {
        "total": "351",
        "ok": "351",
        "ko": "-"
    },
    "percentiles2": {
        "total": "362",
        "ok": "362",
        "ko": "-"
    },
    "percentiles3": {
        "total": "382",
        "ok": "382",
        "ko": "-"
    },
    "percentiles4": {
        "total": "607",
        "ok": "607",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 822,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "15.073",
        "ok": "15.073",
        "ko": "-"
    }
},
contents: {
"req_get-all-user-faeb0": {
        type: "REQUEST",
        name: "get all user",
path: "get all user",
pathFormatted: "req_get-all-user-faeb0",
stats: {
    "name": "get all user",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "98",
        "ok": "98",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles2": {
        "total": "77",
        "ok": "77",
        "ko": "-"
    },
    "percentiles3": {
        "total": "305",
        "ok": "305",
        "ko": "-"
    },
    "percentiles4": {
        "total": "323",
        "ok": "323",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 60,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.091",
        "ok": "1.091",
        "ko": "-"
    }
}
    },"req_get-single-user-fd8e3": {
        type: "REQUEST",
        name: "get single user",
path: "get single user",
pathFormatted: "req_get-single-user-fd8e3",
stats: {
    "name": "get single user",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 60,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.091",
        "ok": "1.091",
        "ko": "-"
    }
}
    },"req_add-user-a646e": {
        type: "REQUEST",
        name: "add user",
path: "add user",
pathFormatted: "req_add-user-a646e",
stats: {
    "name": "add user",
    "numberOfRequests": {
        "total": "709",
        "ok": "709",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "876",
        "ok": "876",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "361",
        "ok": "361",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles1": {
        "total": "354",
        "ok": "354",
        "ko": "-"
    },
    "percentiles2": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "385",
        "ok": "385",
        "ko": "-"
    },
    "percentiles4": {
        "total": "723",
        "ok": "723",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 702,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "12.891",
        "ok": "12.891",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
