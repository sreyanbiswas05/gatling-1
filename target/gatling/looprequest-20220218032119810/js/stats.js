var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4",
        "ok": "4",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "986",
        "ok": "986",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "402",
        "ok": "402",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "363",
        "ok": "363",
        "ko": "-"
    },
    "percentiles1": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "percentiles2": {
        "total": "547",
        "ok": "547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "898",
        "ok": "898",
        "ko": "-"
    },
    "percentiles4": {
        "total": "968",
        "ok": "968",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 75
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 25
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.5",
        "ok": "0.5",
        "ko": "-"
    }
},
contents: {
"req_get-all-user-faeb0": {
        type: "REQUEST",
        name: "get all user",
path: "get all user",
pathFormatted: "req_get-all-user-faeb0",
stats: {
    "name": "get all user",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.125",
        "ok": "0.125",
        "ko": "-"
    }
}
    },"req_get-single-user-fd8e3": {
        type: "REQUEST",
        name: "get single user",
path: "get single user",
pathFormatted: "req_get-single-user-fd8e3",
stats: {
    "name": "get single user",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.125",
        "ok": "0.125",
        "ko": "-"
    }
}
    },"req_add-user-a646e": {
        type: "REQUEST",
        name: "add user",
path: "add user",
pathFormatted: "req_add-user-a646e",
stats: {
    "name": "add user",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "401",
        "ok": "401",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "986",
        "ok": "986",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "694",
        "ok": "694",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "293",
        "ok": "293",
        "ko": "-"
    },
    "percentiles1": {
        "total": "694",
        "ok": "694",
        "ko": "-"
    },
    "percentiles2": {
        "total": "840",
        "ok": "840",
        "ko": "-"
    },
    "percentiles3": {
        "total": "957",
        "ok": "957",
        "ko": "-"
    },
    "percentiles4": {
        "total": "980",
        "ok": "980",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 50
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
