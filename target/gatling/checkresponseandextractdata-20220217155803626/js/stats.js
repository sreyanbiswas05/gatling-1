var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "2",
        "ok": "1",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "66",
        "ok": "268",
        "ko": "66"
    },
    "maxResponseTime": {
        "total": "268",
        "ok": "268",
        "ko": "66"
    },
    "meanResponseTime": {
        "total": "167",
        "ok": "268",
        "ko": "66"
    },
    "standardDeviation": {
        "total": "101",
        "ok": "0",
        "ko": "0"
    },
    "percentiles1": {
        "total": "167",
        "ok": "268",
        "ko": "66"
    },
    "percentiles2": {
        "total": "218",
        "ok": "268",
        "ko": "66"
    },
    "percentiles3": {
        "total": "258",
        "ok": "268",
        "ko": "66"
    },
    "percentiles4": {
        "total": "266",
        "ok": "268",
        "ko": "66"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 50
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 50
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2",
        "ok": "1",
        "ko": "1"
    }
},
contents: {
"req_get-all-users-a-b4388": {
        type: "REQUEST",
        name: "get all users api",
path: "get all users api",
pathFormatted: "req_get-all-users-a-b4388",
stats: {
    "name": "get all users api",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles2": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles3": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    }
}
    },"req_get-single-user-3baf1": {
        type: "REQUEST",
        name: "get single user api",
path: "get single user api",
pathFormatted: "req_get-single-user-3baf1",
stats: {
    "name": "get single user api",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "maxResponseTime": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "meanResponseTime": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "percentiles2": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "percentiles3": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "percentiles4": {
        "total": "66",
        "ok": "-",
        "ko": "66"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "-",
        "ko": "1"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
