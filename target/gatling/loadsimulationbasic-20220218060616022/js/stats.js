var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "15",
        "ok": "0",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "-",
        "ko": "207"
    },
    "maxResponseTime": {
        "total": "803",
        "ok": "-",
        "ko": "803"
    },
    "meanResponseTime": {
        "total": "357",
        "ok": "-",
        "ko": "357"
    },
    "standardDeviation": {
        "total": "192",
        "ok": "-",
        "ko": "192"
    },
    "percentiles1": {
        "total": "238",
        "ok": "-",
        "ko": "238"
    },
    "percentiles2": {
        "total": "449",
        "ok": "-",
        "ko": "449"
    },
    "percentiles3": {
        "total": "733",
        "ok": "-",
        "ko": "733"
    },
    "percentiles4": {
        "total": "789",
        "ok": "-",
        "ko": "789"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.25",
        "ok": "-",
        "ko": "1.25"
    }
},
contents: {
"req_get-single-user-6537b": {
        type: "REQUEST",
        name: "get single user request",
path: "get single user request",
pathFormatted: "req_get-single-user-6537b",
stats: {
    "name": "get single user request",
    "numberOfRequests": {
        "total": "15",
        "ok": "0",
        "ko": "15"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "-",
        "ko": "207"
    },
    "maxResponseTime": {
        "total": "803",
        "ok": "-",
        "ko": "803"
    },
    "meanResponseTime": {
        "total": "357",
        "ok": "-",
        "ko": "357"
    },
    "standardDeviation": {
        "total": "192",
        "ok": "-",
        "ko": "192"
    },
    "percentiles1": {
        "total": "238",
        "ok": "-",
        "ko": "238"
    },
    "percentiles2": {
        "total": "449",
        "ok": "-",
        "ko": "449"
    },
    "percentiles3": {
        "total": "733",
        "ok": "-",
        "ko": "733"
    },
    "percentiles4": {
        "total": "789",
        "ok": "-",
        "ko": "789"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 15,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.25",
        "ok": "-",
        "ko": "1.25"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
