var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "160",
        "ok": "0",
        "ko": "160"
    },
    "minResponseTime": {
        "total": "185",
        "ok": "-",
        "ko": "185"
    },
    "maxResponseTime": {
        "total": "394",
        "ok": "-",
        "ko": "394"
    },
    "meanResponseTime": {
        "total": "305",
        "ok": "-",
        "ko": "305"
    },
    "standardDeviation": {
        "total": "77",
        "ok": "-",
        "ko": "77"
    },
    "percentiles1": {
        "total": "360",
        "ok": "-",
        "ko": "360"
    },
    "percentiles2": {
        "total": "366",
        "ok": "-",
        "ko": "366"
    },
    "percentiles3": {
        "total": "371",
        "ok": "-",
        "ko": "371"
    },
    "percentiles4": {
        "total": "380",
        "ok": "-",
        "ko": "380"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 160,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "-",
        "ko": "5"
    }
},
contents: {
"req_get-single-user-6537b": {
        type: "REQUEST",
        name: "get single user request",
path: "get single user request",
pathFormatted: "req_get-single-user-6537b",
stats: {
    "name": "get single user request",
    "numberOfRequests": {
        "total": "160",
        "ok": "0",
        "ko": "160"
    },
    "minResponseTime": {
        "total": "185",
        "ok": "-",
        "ko": "185"
    },
    "maxResponseTime": {
        "total": "394",
        "ok": "-",
        "ko": "394"
    },
    "meanResponseTime": {
        "total": "305",
        "ok": "-",
        "ko": "305"
    },
    "standardDeviation": {
        "total": "77",
        "ok": "-",
        "ko": "77"
    },
    "percentiles1": {
        "total": "360",
        "ok": "-",
        "ko": "360"
    },
    "percentiles2": {
        "total": "366",
        "ok": "-",
        "ko": "366"
    },
    "percentiles3": {
        "total": "371",
        "ok": "-",
        "ko": "371"
    },
    "percentiles4": {
        "total": "380",
        "ok": "-",
        "ko": "380"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 160,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5",
        "ok": "-",
        "ko": "5"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
